import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ConfigService } from '../config.service';
import { ToastService } from '../toast.service';
import * as _ from 'lodash';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-oauth-google',
  templateUrl: './oauth-google.component.html',
  styleUrls: ['./oauth-google.component.scss']
})
export class OauthGoogleComponent implements OnInit {
  authenticating: any;
  redirect_uri: string;
  apiUrl;
  mode;
  result;

  callback = (e) => {
    if (e && e["origin"] == document.location.origin && e["data"] && e["data"]["type"] == `oauth:google`) {
      this.authenticating = true;
      this.api.get({
        endpoint: this.apiUrl,
        params: {
          code: e["data"]["args"]["code"],
          redirect_uri: this.redirect_uri
        },
      }).subscribe((response: any) => {
        this.result = response;
        window.removeEventListener('message', this.callback);
      }, err => {
        this.authenticating = false;
        window.removeEventListener('message', this.callback);
        this.toast.error(_.get(err, "error.message") || "An unknown error has occured.");
      });
    }
  };

  constructor(
    private api: ApiService,
    private toast: ToastService,
    public config: ConfigService
  ) {
    const baseUrl = document.location.origin;
    this.redirect_uri = baseUrl.includes('localhost') ? baseUrl + '/assets/oauth/google/index.html' :
      baseUrl + '/xano-google-oauth/assets/oauth/google/index.html';
  }

  ngOnInit(): void {
  }

  initOAuth(mode, endpoint) {
    this.mode = mode;
    this.apiUrl = endpoint;

    let oauthWindow = window.open("", `googleOauth`, `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=600,height=660,left=100,top=100`);

    window.removeEventListener('message', this.callback);
    window.addEventListener('message', this.callback);

    this.api.get({
      endpoint: `${this.config.xanoApiUrl}/oauth/google/init`,
      params: {
        redirect_uri: this.redirect_uri
      }
    }).subscribe((response: any) => {
      oauthWindow.location.href = response.authUrl;
    }, err => {
      this.toast.error(_.get(err, "error.message") || "An unknown error has occured.");
      oauthWindow.close();
    });
  }

  loginOAuthGoogle() {
    this.initOAuth('login', `${this.config.xanoApiUrl}/oauth/google/login`);
  }

  signupOAuthGoogle() {
    this.initOAuth('signup', `${this.config.xanoApiUrl}/oauth/google/signup`);
  }

  continueOAuthGoogle() {
    this.initOAuth('continue', `${this.config.xanoApiUrl}/oauth/google/continue`);
  }

  logout() {
    this.result = false;
    this.mode = '';
  }

  getRedirectURI() {
    return this.redirect_uri;
  }
}
